package DailyPractice.A_WeekOne.d_dayFour;


class MyClass {
    public static void func1() {
        System.out.println("I am func 1");
    }

    private static void func2() {
        System.out.println("I am func 2");
    }

    public void func3() {
        System.out.println("I am func 3");
    }

    private void func4() {
        System.out.println("I am func 4");
    }
}

public class Main2 {
    public static void main(String[] args) {
        MyClass obj = new MyClass();

        MyClass.func1();
        //MyClass.func2();
        obj.func1();

        // all private members will not be accessed from outside the class
        // public static can be accessed form the Class level
        // public void can be accessed, but must be the object level

    }

}
