package DailyPractice.A_WeekOne.d_dayFour;

public class Main {

    static int add(int num1, int num2) {
        return num1 + num2;
    }

    static float add(float num1, float num2) {
        return num1 + num2;
    }

    static int add(int num1, int num2, int num3) {
        return num1 + num2 + num3;
    }

    public static void main(String[] args) {

        int add2Ints = add(2, 2);
        System.out.println(add2Ints);

        System.out.println();

        float add2Floats = add(2.5f, 2.0f);
        System.out.println(add2Floats);

        System.out.println();

        int add3Ints = add(5, 5, 5);
        System.out.println(add3Ints);
    }
}
