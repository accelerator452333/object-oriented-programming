package DailyPractice.A_WeekOne.d_dayFour;

class Employee {
    private int age;
    private String name;

    public Employee() {
        System.out.println("Default constructor");
        age = 37;
        name = "Jaime";
    }

    public Employee(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public void setAge(int a) {
        age = a;
    }

    public void setName(String n) {
        name = n;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
}

public class Main3 {

    public static void main(String[] args) {

        Employee emp1 = new Employee();

//        emp1.setAge(37);
//        emp1.setName("Jaime");

        System.out.printf("Name: %s  Age: %d", emp1.getName(), emp1.getAge());
        System.out.println();

        Employee emp2 = new Employee(38, "Krystal");
        System.out.println(emp2.getAge() + " " + emp2.getName());
    }
}
