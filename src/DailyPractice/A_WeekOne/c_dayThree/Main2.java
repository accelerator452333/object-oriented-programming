package DailyPractice.A_WeekOne.c_dayThree;

public class Main2 {
    static void myMethod() {
        System.out.println("Hello World!");
    }

    static void name(String userName) {
        System.out.printf("Hello %s", userName);
    }

    static int doubleNumber(int y) {
        return y * 2;
    }

    static String fullname(String fname, String lname) {
        return fname + " " + lname;
    }

    // Below is overloaded methods.
    // Application will "know" which to use based on the arguments used
    static int add(int num1, int num2) {
        return num1 + num2;
    }

    static float add(float num1, float num2) {
        return num1 + num2;
    }

    public static void main(String[] args) {
        myMethod();


        name("Jaime");
        System.out.println();


        int num = doubleNumber(2);
        System.out.println(num);


        String fullName = fullname("Jaime", "Rodriguez");
        System.out.println(fullName);


        float total = add(4.5f, 2.1f);
        System.out.println(total);
        int total2 = add(5, 7);
        System.out.println(total2);

    }
}
