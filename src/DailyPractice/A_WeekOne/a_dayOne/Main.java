package DailyPractice.A_WeekOne.a_dayOne;

import javax.swing.JOptionPane;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        String name = "Jaime";

        System.out.printf("Hello %s!", name);

        JOptionPane.showMessageDialog(null, "Hello " + name);
    }
}
