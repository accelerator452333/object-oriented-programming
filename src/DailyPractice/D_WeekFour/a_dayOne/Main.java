package DailyPractice.D_WeekFour.a_dayOne;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int count = 0;
        int[] nums = {9, 19, 23, 7, 20};

        System.out.print("Enter a number: ");
        int userNum = input.nextInt();
        input.nextLine();

        for (int num : nums){
            if (userNum < num)
                count++;
        }

        System.out.printf("Number of items larger than %d: %d ", userNum, count);


    }
}
