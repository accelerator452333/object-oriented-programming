package DailyPractice.D_WeekFour.a_dayOne;

public class Main3 {
    public static void displayArray(int[] arr) {
        for (int x : arr) {
            System.out.println(x);
        }
    }

    public static int[] inputArray(){
        int[] x = {1, 2, 3, 4};
        return x;
    }

    public static void main(String[] args) {
//        int[] arr = {5, 10, 20, 30, 40};
//
//        displayArray(arr);

        int[] arr;
        arr = inputArray();

        displayArray(arr);
    }
}
