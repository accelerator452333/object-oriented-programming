package DailyPractice.D_WeekFour.a_dayOne;

public class Main5 {
    public static void main(String[] args) {
        // Sorting
        // Bubble Sort
        int[] arr = {1, 5, 3, 10, 9};
        int n = arr.length - 1;

//        for (int i = 0; i < n; i++) {
//            for (int j = 1; j < n - i; j++) {
//                if (arr[j-1] > arr[j]) {
//                    int temp = arr[j-1];
//                    arr[j-1] = arr[j];
//                    arr[j] = temp;
//                }
//            }
//        }

        for (int i = 0; i < n; i++) {
            boolean done = true;
            for (int j = 0; j < n - i; j++) {
                if (arr[j] > arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    done = false;
                }
            }
            if (done) {
                break;
            }
        }

        for (int x : arr)
            System.out.println(x);
    }
}
