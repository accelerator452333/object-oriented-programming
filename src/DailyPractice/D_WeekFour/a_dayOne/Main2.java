package DailyPractice.D_WeekFour.a_dayOne;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] numItems = {10, 15, 20, 50};
        double[] discount = {.05, .1, .2, .3};

        int user = input.nextInt();
        input.nextLine();

        int index;

        for (int i = 0; i < numItems.length; i++) {
            if (user > numItems[0]) {
                if (user < numItems[i]){
                    index = i - 1;
                    System.out.printf("Purchasing %d item(s) gets you a discount of %,.0f%%", user, discount[index] * 100);
                    break;
                }
            }
            else
                System.out.println("No discount");
        }

        // Instructor solution
//        int sub = numItems.length - 1;
//        while (sub >= 0 && user < numItems[sub])
//            --sub;


    }
}
