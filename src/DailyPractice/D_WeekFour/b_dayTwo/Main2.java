package DailyPractice.D_WeekFour.b_dayTwo;


public class Main2 {

    public static boolean binarySearch(int[] a, int n) {
        int high = a.length - 1;
        int low = 0;
        int mid = (high + low) / 2;
        boolean found = false;

        while (high >= low) {
            if (a[mid] == n)
                return true;

            if (a[mid] > n)
                high = mid - 1;
            else
                low = mid + 1;
            mid = (high + low) / 2;

        }

        return false;
    }

    public static void main(String[] args) {
        int[] nums = {1, 3, 10, 15, 17, 22, 44, 53};

        int num = 23;

        System.out.println(binarySearch(nums, num));



    }


}

