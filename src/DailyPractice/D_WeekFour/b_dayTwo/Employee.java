package DailyPractice.D_WeekFour.b_dayTwo;

public class Employee {
    private int age;
    private String name;

    public Employee(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Employee() {
        this(0, "N/A");
    }

    public String toString() {
        return "Employee{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public static void displayArr(Employee[] arr) {
        for (Employee employee : arr)
            System.out.println(employee);
    }

    public static void bubbleSort(Employee[] arr) {
        int n = arr.length - 1;

        for (int i = 0; i < n; i++) {
            boolean done = true;
            for (int j = 0; j < n - i; j++) {
                if (arr[j].getAge() > arr[j+1].getAge()) {
                    Employee temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    done = false;
                }
            }
            if (done)
                return;
        }
    }

    public int getAge() { return age; }
    public String getName() { return name; }

    public void setAge(int age) { this.age = age; }
    public void setName(String name) { this.name = name; }
}
