package DailyPractice.D_WeekFour.b_dayTwo;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random(19);
        String[] names = {"Jaime", "Ibra", "Ed", "Rasul", "Chris"};
        int length = names.length;
        Employee[] employees = new Employee[length];

        for (int i = 0; i < length; i++) {
            int randAge = rand.nextInt((50-20) + 1) + 20;
            employees[i] = new Employee(randAge, names[i]);
        }

        Employee.displayArr(employees);
        Employee.bubbleSort(employees);
        Employee.displayArr(employees);
    }
}
