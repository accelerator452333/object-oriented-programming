package DailyPractice.B_WeekTwo.a_dayOne;

class Automobile {
    private int mpg;
    private double price;
    private String make;

    public Automobile(int mpg, double price, String make) {
        this.mpg = mpg;
        this.price = price;
        this.make = make;
    }

//    public Automobile() {
//        mpg = 0;
//        price = 0.0;
//        make = "No Make";
//    }

    // Constructor utilizing "this"
    public Automobile() {
        this(0, 0.0, "No Make");
    }



    public void setMpg(int mpg) {
        this.mpg = mpg;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public void setMake(String make) {
        this.make = make;
    }

    public int getMpg() {
        return mpg;
    }
    public double getPrice() {
        return price;
    }
    public String getMake() {
        return make;
    }
}

public class Main {

    public static void main(String[] args) {

        Automobile auto1 = new Automobile();
        auto1.setMpg(35);
        auto1.setPrice(24999.00);
        auto1.setMake("Honda");
        System.out.printf("MPG: %d Price: $%,.2f Make: %s", auto1.getMpg(), auto1.getPrice(), auto1.getMake());


        System.out.println();

        Automobile auto2 = new Automobile(0, 36000.00, "Tesla");
        System.out.printf("MPG: %d Price: $%,.2f Make: %s", auto2.getMpg(), auto2.getPrice(), auto2.getMake());

    }
}
