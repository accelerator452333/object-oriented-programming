package DailyPractice.B_WeekTwo.b_dayTwo;

public class Main3 {
    public static void main(String[] args) {

        Tire tire1 = new Tire("Michellen", 200.00);
        Auto auto1 = new Auto(tire1, "Honda");

        System.out.println("Tire Make: " + auto1.getTire().getTireMake() + "\n"
                            + "Tire Price: " + auto1.getTire().getTirePrice() + "\n"
                            + "Car Make: " + auto1.getCarMake());
    }
}
