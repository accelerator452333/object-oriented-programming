package DailyPractice.B_WeekTwo.b_dayTwo;

public class Student {

    private String name;
    private int age;
    private float gpa;
    private static int count = 0;

    public Student(String name, int age, float gpa) {
        this.name = name;
        this.age = age;
        this.gpa = gpa;
        count = count + 1;
    }

    public Student(String name) {
        this(name, 0, 0.0f);
    }

    public Student() {
        this("N/A", 0, 0.0f);
    }

    public void setName(String name) { this.name = name; }
    public void setAge(int age) { this.age = age; }
    public void setGpa(float gpa) { this.gpa = gpa; }

    public String getName() { return name; }
    public int getAge() { return age; }
    public float getGpa() { return gpa; }

    public static int getCount() { return count; }
}
