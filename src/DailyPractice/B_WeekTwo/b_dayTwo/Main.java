package DailyPractice.B_WeekTwo.b_dayTwo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
//
        System.out.print("Enter the name: ");
        String name = userInput.nextLine();

        System.out.print("Enter the age: ");
        int age = userInput.nextInt();

        System.out.print("Enter the GPA: ");
        float gpa = userInput.nextFloat();
        System.out.println();
//
        Student student1 = new Student(name, age, gpa);
        Student student2 = new Student();
        Student student3 = new Student("Krystal");
//
//
        System.out.printf("Name: %s Age: %d GPA: %.1f", student1.getName(), student1.getAge(), student1.getGpa());
        System.out.println();
        System.out.printf("Name: %s Age: %d GPA: %.1f", student2.getName(), student2.getAge(), student2.getGpa());
        System.out.println();
        System.out.printf("Name: %s Age: %d GPA: %.1f", student3.getName(), student3.getAge(), student3.getGpa());
        System.out.println();

        int count = Student.getCount();
        System.out.println(count);

        System.out.println("Test");
    }
}
