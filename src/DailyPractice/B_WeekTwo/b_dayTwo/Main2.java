package DailyPractice.B_WeekTwo.b_dayTwo;

import java.lang.*;
import java.time.*;

public class Main2 {

    public static void main(String[] args) {
        int x = -3;
        double y = 5.21;

        System.out.println(Math.abs(x));
        System.out.println(Math.ceil(y));

        System.out.println();

        LocalDate today = LocalDate.now();
        System.out.println(today);

        LocalDateTime todayTime = LocalDateTime.now();
        System.out.println(todayTime);

        LocalDate anyDate = LocalDate.of(2017, 03, 19);
        System.out.println(anyDate);
    }

}
