package DailyPractice.B_WeekTwo.b_dayTwo;

public class Auto {

    private Tire tire;
    private String carMake;

    public Auto(Tire tire, String carMake) {
        this.tire = tire;
        this.carMake = carMake;
    }

    public Auto() {
        this(new Tire(), "No Auto Make");
    }

    public void setCarMake(String carMake) { this.carMake = carMake; }
    public void setTire(Tire tire) { this.tire = tire; }

    public String getCarMake() { return carMake; }
    public Tire getTire() { return tire; }





}