package DailyPractice.B_WeekTwo.c_dayThree;

import java.util.Scanner;

public class Main3 {

    public static void func1() {
        System.out.println("func1");
    }

    public static void func2() {
        System.out.println("func2");
    }

    public static void func3() {
        System.out.println("func3");
    }

    public static void main(String[] args) {
//        int day = 3;
//
//        switch (day) {
//            case 1 -> System.out.println("Monday");
//            case 2 -> System.out.println("Tuesday");
//            case 3 -> System.out.println("Wednesday");
//            case 4 -> System.out.println("Thursday");
//            case 5 -> System.out.println("Friday");
//            default -> System.out.println("Weekend");
//        }

        Scanner input = new Scanner(System.in);

        System.out.println("Choose A for func1 \n"
                            + "Choose B for func2 \n"
                            + "Choose C for func3");
        System.out.println();
        System.out.print("Enter your choice: ");

        char letter = input.next().charAt(0);
        char finalLetter = Character.toUpperCase(letter);

        switch (finalLetter) {
            case 'A' -> func1();
            case 'B' -> func2();
            case 'C' -> func3();
        }


    }
}
