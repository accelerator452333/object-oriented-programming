package DailyPractice.B_WeekTwo.c_dayThree;

public class Tire {

    private String tireMake;
    private double tirePrice;

    public Tire(String make, double price) {
        this.tireMake = make;
        this.tirePrice = price;
    }

    public Tire() {
        this("Nothing", 0.0);
    }

    public String toString() {
        return tireMake;
    }

    public void setTireMake(String make) { this.tireMake = make; }
    public void setTirePrice(double price) { this.tirePrice = price; }

    public String getTireMake() { return tireMake; }
    public double getTirePrice() { return tirePrice; }
}
