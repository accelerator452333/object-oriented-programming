package DailyPractice.B_WeekTwo.c_dayThree;

import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter name: ");
        String name = userInput.nextLine();

        System.out.print("Enter your score: ");
        int score = userInput.nextInt();

        Student student1 = new Student(name, score);

        String letter = student1.letterGrade();
        System.out.println(letter);


    }
}
