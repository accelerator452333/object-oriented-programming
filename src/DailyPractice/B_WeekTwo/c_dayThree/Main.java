package DailyPractice.B_WeekTwo.c_dayThree;

public class Main {

    public static void main(String[] args) {

        int grade = 85;

        if (grade >= 90) {
            System.out.println("A");
        } else if (grade >= 80) {
            System.out.println("B");
        } else {
            System.out.println("C");
        }


        // if grade is >= 70 , output equals good , otherwise equal bad
        String output = (grade >= 70) ? "Good" : "Bad";
        System.out.println(output);


        int x = 10;
        int y = 20;

        if (x > 10 && y > 20) {
            System.out.println("Hello");
        } else {
            System.out.println("World");
        }
    }
}
