package DailyPractice.B_WeekTwo.c_dayThree;

public class Student {

    private String name;
    private int score;

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public Student() {
        this("None", 0);
    }

    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grade=" + score +
                '}';
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getScore() { return score; }
    public void setScore(int score) {
        this.score = score;
    }

    public String letterGrade() {
        if (score >= 90)
            return "A";
        else if (score >= 80)
            return "B";
        else if (score >= 70)
            return "C";
        else if (score >= 60)
            return "D";
        else
            return "F";
   }

}
