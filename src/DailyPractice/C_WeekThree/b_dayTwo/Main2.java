package DailyPractice.C_WeekThree.b_dayTwo;
import java.util.Scanner;

public class Main2 {

    public static void displayMenu() {
        System.out.println();
        System.out.println("1- Calculate Average");
        System.out.println("2- Calculate Minimum");
        System.out.println("3- Calculate Maximum");
        System.out.println("4- Exit");
    }

    public static void calcAverage(int numbers) {
        Scanner input = new Scanner(System.in);
        while (numbers <= 0) {
            System.out.println("Must a number greater than 1.");
            System.out.print("Enter number of digits to average: ");
            numbers = input.nextInt();
            input.nextLine();
        }
        int sum = 0;
        for (int i = 1; i < numbers+1; i++) {
            System.out.printf("Enter number %d: ", i);
            int num = input.nextInt();
            sum += num;
        }
        int average = sum / numbers;
        System.out.printf("Average: %d ", average);
    }

    public static int calcMinimum() {
        System.out.println("Calculate Minimum");
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int num = input.nextInt();
        input.nextLine();

        while (num == -99) {
            System.out.println("First number can not be -99.");
            System.out.print("Enter a number: ");
            num = input.nextInt();
            input.nextLine();
        }
        int minNum = num;
        while (num != -99) {
            if (num < minNum) {
                minNum = num;
            }
            System.out.print("Enter another number or -99 to quit: ");
            num = input.nextInt();
            input.nextLine();
        }
        return minNum;
    }

    public static void calcMaximum() {
        System.out.println("Calculate Maximum");
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int num = input.nextInt();
        input.nextLine();

        while (num == 99) {
            System.out.println("First number can not be 99.");
            System.out.print("Enter a number: ");
            num = input.nextInt();
            input.nextLine();
        }
        int maxNum = num;
        while (num != 99) {
            if (num > maxNum) {
                maxNum = num;
            }
            System.out.print("Enter another number or 99 to quit: ");
            num = input.nextInt();
            input.nextLine();
        }
        System.out.printf("Maximum Number: %d", maxNum);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice;

        do {
            displayMenu();
            System.out.print("Please enter your choice: ");
            choice = input.nextInt();
            input.nextLine();

            switch (choice){
                case 1 -> {
                    System.out.println("Calculate Average");
                    System.out.print("Enter number of digits to average: ");
                    int nums = input.nextInt();
                    input.nextLine();
                    calcAverage(nums);
                    System.out.println();
                }
                case 2 -> System.out.printf("Minimum Number: %d", calcMinimum());
                case 3 -> calcMaximum();
                case 4 -> System.out.println("Exiting");
                default -> System.out.print("Bad Input");
            }
        }
        while (choice != 4);
    }
}
