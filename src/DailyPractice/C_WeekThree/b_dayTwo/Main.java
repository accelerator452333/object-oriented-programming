package DailyPractice.C_WeekThree.b_dayTwo;
import java.util.Scanner;

public class Main {

    public static void displayMenu(){
        System.out.println();
        System.out.println("1- Default Constructor");
        System.out.println("2- Non-Default Constructor");
        System.out.println("3- Name-Only Constructor");
        System.out.println("4- Exit");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choice;
        String name;
        int salary;

        do {
            displayMenu();
            System.out.print("Please enter your choice: ");
            choice = input.nextInt();
            input.nextLine();

            switch (choice) {
                case 1 -> {
                    System.out.println("Defualt");
                    System.out.print("Please Enter the name: ");
                    name = input.nextLine();
                    System.out.print("Please enter the age: ");
                    salary = input.nextInt();
                    input.nextLine();
                    Employee e = new Employee();
                    e.setName(name);
                    e.setSalary(salary);
                    System.out.println(e);
                }
                case 2 -> {
                    System.out.println("Non-Default");
                    System.out.print("Please Enter the name: ");
                    name = input.nextLine();
                    System.out.print("Please enter the age: ");
                    salary = input.nextInt();
                    input.nextLine();
                    Employee e2 = new Employee(name, salary);
                    System.out.println(e2);
                }
                case 3 -> {
                    System.out.println("Name-Only");
                    System.out.print("Please Enter the name: ");
                    name = input.nextLine();
                    Employee e3 = new Employee(name);
                    System.out.println(e3);
                }
                case 4 -> System.out.println("Exiting");
                default -> System.out.println("Bad Input");
            }
        }
        while (choice != 4);
    }
}
