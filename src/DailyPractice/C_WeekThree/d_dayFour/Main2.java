package DailyPractice.C_WeekThree.d_dayFour;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        final int NUM_ENTRIES = 5;

        int[] nums = new int[NUM_ENTRIES];

        for(int i = 0; i < nums.length; i++) {
            System.out.print("Enter a number: ");
            nums[i]  = input.nextInt();
            input.nextLine();


        }

        for(int num : nums)
            System.out.println(num);

    }
}
