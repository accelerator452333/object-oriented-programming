package DailyPractice.C_WeekThree.d_dayFour;

import java.util.Scanner;

public class Main5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] nums = {3, 9, 23, 19, 7};
        int idx = 0;
        boolean found = false;
        System.out.print("Enter a number to search: ");
        int userNum = input.nextInt();

        do {
            for (int i = 0; i < nums.length; i++ ) {
                if (userNum == nums[i]) {
                    idx = i;
                    found = true;
                    break;
                }
            }

            if (found == true) {
                System.out.println(userNum + " found at index " + idx);

            }
            else
                System.out.println("Not found");

            System.out.print("Enter a number to search or -99 to quit: ");
            userNum = input.nextInt();
        } while (userNum != -99);









    }
}
