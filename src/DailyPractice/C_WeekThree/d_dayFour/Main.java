package DailyPractice.C_WeekThree.d_dayFour;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Define an array
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        System.out.println(cars[0]);

        cars[0] = "Opel";

        for (String car : cars)
            System.out.println(car);

        int[] nums = {10, 20, 30, 40};


        Scanner input = new Scanner(System.in);
        nums[1] = input.nextInt();

        System.out.println(nums[1]);


        double[] arr1;
        // Array will hold 20 items
        arr1 = new double[20];



    }
}
