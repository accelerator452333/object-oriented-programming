package DailyPractice.C_WeekThree.d_dayFour;

class Employee {
    private int id;
    private String name;

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Employee() {
        this(0, "Nothing");
    }

    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

public class Main4 {

    public static void main(String[] args) {

        Employee[] empArr = new Employee[5];

        for (int i = 0; i < empArr.length; i++) {
            empArr[i] = new Employee(i, "Jaime");
        }

        for (Employee emp : empArr)
            System.out.println(emp);
    }
}
