package DailyPractice.C_WeekThree.c_dayThree;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {

        // Work with String Class

        String aname = "Carmen";
        String anotherName;

        Scanner input = new Scanner(System.in);
        System.out.print("Enter your name: ");
        anotherName = input.nextLine();

        // When using a variable and input they will not equal
        if(aname == anotherName)
            System.out.println("Equal");
        else
            System.out.println("Not Equal");

        // Use when comparing strings from input
        if(aname.equals(anotherName))
            System.out.println("Equal");
        else
            System.out.println("Not Equal");

        if(anotherName.equalsIgnoreCase(anotherName))
            System.out.println(aname + " = " + anotherName);
        else
            System.out.println(aname + " != " + anotherName);
    }
}
