package DailyPractice.C_WeekThree.c_dayThree;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Enter your full name: ");
        String name = input.nextLine();

        int space = name.indexOf(" ");
        String fname = name.substring(0, space);
        String lname = name.substring(space+1, name.length());
        System.out.println(fname);
        System.out.println(lname);
    }
}
