package DailyPractice.C_WeekThree.c_dayThree;

public class Main5 {
    public static void main(String[] args) {

//        StringBuilder name = new StringBuilder("Jaime");
//
//        StringBuilder reversed = name.reverse();
//        System.out.println(reversed);
//
//        name = name.append("Rodriguez");
//        System.out.println(name);

        StringBuilder nameString = new StringBuilder("Barbera");

        int capacity = nameString.capacity();
        System.out.println(capacity);

        nameString.setLength(4);
        System.out.println(nameString);
    }
}
