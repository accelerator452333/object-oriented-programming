package DailyPractice.C_WeekThree.c_dayThree;

public class Main4 {
    public static void main(String[] args) {

        int decimal = Integer.parseInt("20");
        System.out.println(decimal);

        int signedPositive = Integer.parseInt("+20");
        System.out.println(signedPositive);

        int signedNegative = Integer.parseInt("-20");
        System.out.println(signedNegative);

        int radix = Integer.parseInt("20", 16);
        System.out.println(radix);

        int string = Integer.parseInt("geeks", 29);
        System.out.println(string );
    }
}
