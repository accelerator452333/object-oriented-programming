package DailyPractice.C_WeekThree.a_dayOne;

public class Main {

    public static void main(String[] args) {
        // While loop
//        int x = 0;
//
//        while (x < 10) {
//            System.out.println(x);
//             x += 1;
//        }


        // Do While
        int x = 0;
        do {
            System.out.println(x);
            x++;
        }
        while (x < 10);
    }
}
