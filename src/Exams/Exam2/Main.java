package Exams.Exam2;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random random = new Random(19);

        final int maxAge = 50;
        final int minAge = 18;
        final int minGpa = 2;
        final int maxGpa = 4;

        // Step 2: Create an array of Students with random ages and GPA's.
        Student[] students = new Student[8];
        for (int i = 0; i < students.length; i++) {
            int age = random.nextInt(maxAge - minAge + 1) + minAge;
            double gpa = random.nextInt(maxGpa - minGpa + 1) + minGpa;
            students[i] = new Student(age, gpa);
        }

        // Step 2: Display Students.
        Student.displayStudents(students);

        System.out.println();

        // Setp 3: Generare random number between 0 and array length.
        int randomNum = random.nextInt((students.length - 1) - 0 + 1);

        // Step 4: Dispaly "Student" at index of randomly generated number.
        Student randomStudent = Student.selectByIndex(students, randomNum);
        System.out.println("Randomly selected Student:");
        System.out.println(randomStudent);

        System.out.println();

        // Step 5-7: Split by the age of the random student. Sort arrays. Combine all elements back together sorted.
        int randomAge = randomStudent.getAge();
        Student[] sortedStudents = Student.splitArray(students, randomNum);
        Student.displayStudents(sortedStudents);

        // Step 8: Accept age from user
        System.out.println();
        System.out.print("Enter an age to search: ");
        int userAge = input.nextInt();
        input.nextLine();

        // Step 9: Search for users age in array of Students and return whether found or not.
        Student.binarySearch(sortedStudents, userAge);





    }
}