package Exams.Exam2;

public class Student {
    private int age;
    private double gpa;

    public Student(int age, double gpa) {
        this.age = age;
        this.gpa = gpa;
    }

    public Student() {
        this(0, 0.0);
    }

    public String toString() {
        return "Student{" +
                "age=" + age +
                ", gpa=" + gpa +
                '}';
    }

    public static void displayStudents(Student[] arr) {
        System.out.println("Array Contains the following items:");
        for (Student a : arr)
            System.out.println(a);
    }

    public static Student selectByIndex(Student[] arr, int num) {
        return arr[num];
    }

    public static Student[] bubbleSort(Student[] arr) {
        int n = arr.length - 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                if (arr[j].getAge() > arr[j + 1].getAge()) {
                    Student temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;
    }

    public static Student[] splitArray(Student[] arr, int num){
        int n = arr.length;
        int splitAge = arr[num].getAge();
        int smaller = 0;
        int larger = 0;
        int incr1 = 0;
        int incr2 = 0;

        for(int i = 0; i < n; i++) {
            if (arr[i].getAge() < splitAge) {
                smaller++;
            }
            else if (arr[i].getAge() > splitAge) {
                larger++;
            }
        }

        Student[] lowerArr = new Student[smaller];
        Student[] upperArr = new Student[larger];
        for (int i = 0; i < n; i++) {
            if (arr[i].getAge() < splitAge){
                lowerArr[incr1] = arr[i];
                incr1++;
            }
            else if (arr[i].getAge() > splitAge) {
                upperArr[incr2] = arr[i];
                incr2++;
            }
        }

        Student.bubbleSort(lowerArr);
        Student.bubbleSort(upperArr);

        Student[] combinedArray = new Student[lowerArr.length + upperArr.length];
        for (int i = 0; i < lowerArr.length; i++) {
            combinedArray[i] = lowerArr[i];
        }

        combinedArray[lowerArr.length] = arr[num];

        int incr3 = 0;
        for (int i = lowerArr.length + 1; i < combinedArray.length; i++) {
            combinedArray[i] = upperArr[incr3];
            incr3++;
        }
        return combinedArray;
    }

    public static void binarySearch(Student[] arr, int num) {
        int high = arr.length - 1;
        int low = 0;
        int mid = (high + low) / 2;
        boolean found = false;

        while (high >= low) {
            if (arr[mid].getAge() == num){
                found = true;
                break;
            }
            if (arr[mid].getAge() > num)
                high = mid - 1;
            else
                low = mid + 1;
            mid = (high + low) / 2;

        }

        if (found == true)
            System.out.println("Found");
        else
            System.out.println("Not Found");
    }

    public int getAge() { return age; }
    public double getGpa() { return gpa; }

    public void setAge(int age) { this.age = age; }
    public void setGpa(double gpa) { this.gpa = gpa; }
}
