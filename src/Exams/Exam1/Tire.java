package Exams.Exam1;

public class Tire {
    private double price;
    private String make;
    private int mileage;

    public Tire(double price, String make, int mileage) {
        this.price = price;
        this.make = make;
        this.mileage = mileage;
    }

    public Tire() {
        this(0.0, "none", 0);
    }

    public String toString() {
        return "Tire{" +
                "price=" + price +
                ", make='" + make + '\'' +
                ", mileage=" + mileage +
                '}';
    }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }

    public String getMake() { return make; }
    public void setMake(String make) { this.make = make; }

    public int getMileage() { return mileage; }
    public void setMileage(int mileage) { this.mileage = mileage;}
}
