package Exams.Exam1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Object 1
        Scanner input = new Scanner(System.in);
        System.out.print("Enter PRICE of Tire 1: ");
        double tire1Price = input.nextDouble();
        input.nextLine();

        if (tire1Price < 0) {
            System.out.println("Tire price can not be less than 0.");
            System.out.print("Enter PRICE of Tire 1: ");
            tire1Price = input.nextDouble();
            input.nextLine();
        }
        System.out.print("Enter MAKE of Tire 1: ");
        String tire1Make = input.nextLine();

        System.out.print("Enter MILEAGE of Tire 1: ");
        int tire1Mileage = input.nextInt();
        input.nextLine();
        if (tire1Mileage <= 5000) {
            System.out.println("Tire mileage must be greater than 5,000");
            System.out.print("Enter MILEAGE of Tire 1: ");
            tire1Mileage = input.nextInt();
            input.nextLine();
        }

        System.out.print("Enter MAKE of Car 1: ");
        String auto1Make = input.nextLine();

        System.out.print("Enter PRICE of Car 1: ");
        double auto1Price = input.nextDouble();
        input.nextLine();
        if (auto1Price < 0) {
            System.out.println("Car price must be great than 0.");
            System.out.print("Enter PRICE of Car 1: ");
            auto1Price = input.nextDouble();
            input.nextLine();
        }

        Tire tire1 = new Tire();
        Automobile auto1 = new Automobile();

        tire1.setPrice(tire1Price);
        tire1.setMake(tire1Make);
        tire1.setMileage(tire1Mileage);
        auto1.setMake(auto1Make);
        auto1.setPrice(auto1Price);
        auto1.setTire(tire1);

        Automobile.autoCount();


        // Object 2
        System.out.println();
        System.out.print("Enter PRICE of Tire 2: ");
        double tire2Price = input.nextDouble();
        input.nextLine();

        if (tire2Price < 0) {
            System.out.println("Tire price can not be less than 0.");
            System.out.print("Enter PRICE of Tire 2: ");
            tire2Price = input.nextDouble();
            input.nextLine();
        }
        System.out.print("Enter MAKE of Tire 2: ");
        String tire2Make = input.nextLine();

        System.out.print("Enter MILEAGE of Tire 2: ");
        int tire2Mileage = input.nextInt();
        input.nextLine();
        if (tire2Mileage <= 5000) {
            System.out.println("Tire mileage must be greater than 5,000");
            System.out.print("Enter MILEAGE of Tire 2: ");
            tire2Mileage = input.nextInt();
            input.nextLine();
        }

        System.out.print("Enter MAKE of Car 2: ");
        String auto2Make = input.nextLine();

        System.out.print("Enter PRICE of Car 2: ");
        double auto2Price = input.nextDouble();
        input.nextLine();
        if (auto2Price < 0) {
            System.out.println("Car price must be great than 0.");
            System.out.print("Enter PRICE of Car 2: ");
            auto2Price = input.nextDouble();
            input.nextLine();
        }

        Tire tire2 = new Tire(tire2Price, tire2Make, tire2Mileage);
        Automobile auto2 = new Automobile(auto2Make, auto2Price, tire2);
        Automobile.autoCount();



        // Object 3
        System.out.println();
        System.out.print("Enter PRICE of Tire 3: ");
        double tire3Price = input.nextDouble();
        input.nextLine();

        if (tire3Price < 0) {
            System.out.println("Tire price can not be less than 0.");
            System.out.print("Enter PRICE of Tire 3: ");
            tire3Price = input.nextDouble();
            input.nextLine();
        }
        System.out.print("Enter MAKE of Tire 3: ");
        String tire3Make = input.nextLine();

        System.out.print("Enter MILEAGE of Tire 3: ");
        int tire3Mileage = input.nextInt();
        input.nextLine();
        if (tire3Mileage <= 5000) {
            System.out.println("Tire mileage must be greater than 5,000");
            System.out.print("Enter MILEAGE of Tire 3: ");
            tire3Mileage = input.nextInt();
            input.nextLine();
        }

        System.out.print("Enter MAKE of Car 3: ");
        String auto3Make = input.nextLine();

        System.out.print("Enter PRICE of Car 3: ");
        double auto3Price = input.nextDouble();
        input.nextLine();
        if (auto3Price < 0) {
            System.out.println("Car price must be great than 0.");
            System.out.print("Enter PRICE of Car 3: ");
            auto3Price = input.nextDouble();
            input.nextLine();
        }

        Tire tire3 = new Tire(tire3Price, tire3Make, tire3Mileage);
        Automobile auto3 = new Automobile(auto3Make, auto3Price, tire3);
        Automobile.autoCount();


        // Find and Print object with max tire price
        double maxTirePrice = Math.max(Math.max(tire1.getPrice(), tire2.getPrice()), tire3.getPrice());
        System.out.println();
        System.out.println("Max Tire Price - Tire Info: ");
        if (maxTirePrice == tire1.getPrice()) {
            System.out.println(tire1);
        } else if (maxTirePrice == tire2.getPrice()) {
            System.out.println(tire2);
        } else {
            System.out.println(tire3);
        }


        // Find and Print object with min car price
        double minCarPrice = Math.min(Math.min(auto1.getPrice(), auto2.getPrice()), auto3.getPrice());
        System.out.println();
        System.out.println("Min Car Price - Car Info: ");
        if (minCarPrice == auto1.getPrice()) {
            System.out.println(auto1);
        } else if (minCarPrice == auto2.getPrice()) {
            System.out.println(auto2);
        } else {
            System.out.println(auto3);
        }
    }
}