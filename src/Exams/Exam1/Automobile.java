package Exams.Exam1;

public class Automobile {

    private String make;
    private double price;
    private Tire tire;
    private static int count = 0;

    public Automobile(String make, double price, Tire tire) {
        this.make = make;
        this.price = price;
        this.tire = tire;
        count++;
    }

    public Automobile() {
        this("none", 0.0, new Tire());
    }

    public String toString() {
        return "Automobile{" +
                "make='" + make + '\'' +
                ", price=" + price +
                ", tire=" + tire +
                '}';
    }

    public static void autoCount() {
        System.out.printf("CAR OBJECTS MADE: %d ", count);
        System.out.println();
    }

    public static int getCount() { return count; }
    public static void setCount(int count) { Automobile.count = count; }

    public String getMake() { return make; }
    public void setMake(String make) { this.make = make; }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }

    public Tire getTire() { return tire; }
    public void setTire(Tire tire) { this.tire = tire; }
}
